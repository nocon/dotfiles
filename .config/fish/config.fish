if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_prompt
  if test $status -eq 0
    set_color green; echo -ne "●"; set_color normal
  else 
    set_color red; echo -ne "●"; set_color normal
  end
  echo -ne " $(pwd) > "
end