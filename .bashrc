source ~/dotfiles/shared.sh 


# silence the mac warning about deprecation
export BASH_SILENCE_DEPRECATION_WARNING=1

# customise the prompt
red=$(tput setaf 1)
green=$(tput setaf 2)
normal=$(tput sgr0)
PS1="\$([ \$? == 0 ] && printf '${green}●${normal}' || printf '${red}●${normal}' ) \w > "

# rename the window to current directory
set-window-title() {
  echo -en "\033]0;$(pwd | sed -e "s;^$HOME;~;")\a"
}

# add a new line after each finished command, but not before the first line in the terminal
PROMPT_COMMAND="echo;set-window-title"

# append bash history, don't overwrite
shopt -s histappend
