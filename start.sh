#!/bin/bash

# show files and directories starting with .
defaults write com.apple.finder AppleShowAllFiles true
killall Finder

. ./helpers.sh

update

install git
install curl
install jq
install wget
install ssh

install npm
install python

install csvkit

install autojump

install zsh

# install-app hyper
# pin "Hyper"
# ln -sf "$DOTFILES_DIR/hyper/.hyper.js" ~

install-app visual-studio-code-insiders
pin "Visual Studio Code - Insiders"
ln -sf $DOTFILES_DIR/vscode/settings.json $HOME/Library/Application Support/Code/User/settings.json

install-app brave-browser
pin "Brave Browser"

ln -sf "$DOTFILES_DIR/.bashrc" ~
ln -sf "$DOTFILES_DIR/.zshrc" ~
ln -sf "$DOTFILES_DIR/.wezterm.lua" ~

# change default terminal to bash
# chsh -s /bin/bash